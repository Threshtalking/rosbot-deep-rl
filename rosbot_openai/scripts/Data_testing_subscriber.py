#!/usr/bin/env python
import rospy
from sensor_msgs.msg import LaserScan
from sensor_msgs.msg import Image
from sensor_msgs.msg import PointCloud2
from sensor_msgs.msg import BatteryState
from openai_ros.robot_envs import husarion_env



def listener():
    # In ROS, nodes are uniquely named. If two nodes with the same
    # name are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.
    rospy.init_node('listener', anonymous=True)
    TEST_ONE_TIME = True
    # Creating the different subscribers

    #Laserscan
    #rospy.Subscriber('/scan', LaserScan, laser_scan_callback)

    #RGB Image
    #rospy.Subscriber('/camera/rgb/image_raw', Image, camera_depth_image_raw_callback)

    #Pointcloud
    #rospy.Subscriber('/camera/depth/points', PointCloud2, camera_depth_pointcloud_callback)

    #rospy.Subscriber('/base_scan', LaserScan, degree_callback)

    #BatteryState
    rospy.Subscriber('/battery', BatteryState, battery_callback)
    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()

def battery_callback(data):
    rospy.loginfo("Battery State: %s", data.present)

def degree_callback(data):
    rospy.loginfo("LASERDATA 180 DEGREES: %s", data.angle_min)

def laser_scan_callback(data):
    rospy.loginfo("LASERDATA: %s", data.angle_min)


def camera_depth_image_raw_callback(data):
    rospy.loginfo("RGB IMAGEDATA: %s", data.height)

def camera_depth_pointcloud_callback(data):
    rospy.loginfo("POINTCLOUD DATA: %s", data)

if __name__ == '__main__':
        listener()

